const { Model, DataTypes } = require('sequelize');

/**
 * @param { sequelize } sequelize Instancia do sequelize conectada.
 */
module.exports = (sequelize) => {
    class Autor extends Model { }
    Autor.init({
        idAutor: {
            type: DataTypes.INTEGER.UNSIGNED,
            autoIncrement: true,
            primaryKey: true,
        },
        nome: {
            type: DataTypes.TEXT,
            allowNull: false,
        },
    }, {
            tableName: 'autor',
            sequelize: sequelize,
        }
    );


    /**
     * @param {any} models Lista de modelos do banco.
     */
    Autor.associations = (models) => {
        Autor.hasMany(models.livro, {
            foreignKey: 'idLivro',
        });
    }

    return Autor;
}