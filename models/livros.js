const { Model, DataTypes } = require('sequelize');

/**
 * @param { sequelize } sequelize Instancia do sequelize conectada.
 */
module.exports = (sequelize) => {
    class Livro extends Model { }
    Livro.init({
        idLivro: {
            type: DataTypes.INTEGER.UNSIGNED,
            autoIncrement: true,
            primaryKey: true,
        },
        nome: {
            type: DataTypes.TEXT,
            allowNull: false,
        },
    }, {
            tableName: 'livro',
            sequelize: sequelize,
        }
    );

    /**
     * @param {Sequelize.Models} models Lista de modelos do banco.
     */
    Livro.associations = (models) => {
        Livro.belongsTo(models.autor, {
            foreignKey: 'idAutor',
        });
    }
    return Livro;
}