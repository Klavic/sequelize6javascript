const { Sequelize, Model, DataTypes } = require('sequelize');

const Autor = require('./models/autor');
const Livro = require('./models/livros');

// Instância do sequelize
const sequelize = new Sequelize('postgres://postgres:postgres@127.0.0.1:5432/teste', { logging: false });

// Carregando modelos.
const models = {};
models.autor = Autor(sequelize);
models.livro = Livro(sequelize);

// models.autor.associations(models);
// models.livro.associations(models);

// Associações
Object.keys(models).forEach((pModel) => {
    console.log(pModel)
    if (models[pModel].associations !== undefined) {
        models[pModel].associations(models);
    }
});


(async () => {
    try {
        await sequelize.sync({ force: true });

        const autor = await models.autor.create({
            nome: 'Autor 1',
        });

        await models.livro.create({
            nome: 'livro 1',
            idAutor: autor.get({ plain: true }).idAutor,
        });

        const livros = await models.livro.findAll({
            include: [{ model: models.autor }]
        });

        const livrosLimpos = livros.map(pLiv => pLiv.get({ plain: true }));
        console.log(livrosLimpos);

    } catch (err) {
        console.log(err);
    }
})();
